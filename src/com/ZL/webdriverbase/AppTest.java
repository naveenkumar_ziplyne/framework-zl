package com.ZL.webdriverbase;

import java.io.File;
import java.util.Locale;

import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import com.ZL.utils.CommonUtils;
import com.ZL.webdriverFactory.AppDriver;

@Listeners(AppDriver.class)
public class AppTest extends AppDriver {

	CommonUtils utils = new CommonUtils();

	@BeforeSuite(alwaysRun = true)
	public void beforeSuite(ITestContext ctx) {
		if (Boolean.parseBoolean(System.getProperty("userdocker", "false"))) {
			File fi = null;
			fi = new File("output.txt");
			if (fi.delete()) {
				System.out.println("file deleted successfully");
			}
		}
		if (!Boolean.valueOf(System.getProperty("grid", "false").toLowerCase(Locale.ENGLISH)))
			setDriverExecutable();
		logger.info("XML FileName : " + ctx.getCurrentXmlTest().getSuite().getFileName());
		logger.info("Executing the Suite : " + ctx.getSuite().getName());
	}

	@AfterSuite(alwaysRun = true)
	public void AfterSuite() {
	}

}
